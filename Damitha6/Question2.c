#include<stdio.h>
int fibonacciSeq(int a);

int main(void)
{
    int seqval;

    printf("Enter the sequence no:\n ");
    scanf("%d", &seqval);

    for(int a = 0; a <=seqval; a++)
    {
        printf("%d \n", fibonacciSeq(a));
    }

    return 0;
}

int fibonacciSeq(int a)
{
    if(a == 0 || a == 1)
    {
        return a;
    }

    else

    {
        return fibonacciSeq(a-1) + fibonacciSeq(a-2);
    }

}